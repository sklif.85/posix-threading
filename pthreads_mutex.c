//
// Created by alex on 14.02.18.
//

#include "threadings.h"
#define NTHREADS 10

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
int  counter = 0;

void *func()
{
    printf("Thread number %ld\n", pthread_self());
    pthread_mutex_lock( &mutex1 );
    counter++;
    pthread_mutex_unlock( &mutex1 );

}

void pthreads_mutex()
{
    pthread_t thread_id[NTHREADS];

    int i, j;

    for(i=0; i < NTHREADS; i++)
        pthread_create(&thread_id[i], NULL, &func, NULL);

    for(j=0; j < NTHREADS; j++)
        pthread_join( thread_id[j], NULL);

    printf("Final counter value: %d\n", counter);

    exit(0);
}