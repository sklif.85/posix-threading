//
// Created by alex on 14.02.18.
//


#ifndef POSIX_THREADING_THREADINGS_H
#define POSIX_THREADING_THREADINGS_H

#include <pthread.h>
#include <stdio.h>
#include <thread_db.h>
#include <stdlib.h>

int main();
void pthreads();
void pthreads_mutex();
void condition_variables();

#endif //POSIX_THREADING_THREADINGS_H
