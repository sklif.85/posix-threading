//
// Created by alex on 14.02.18.
//


#include "threadings.h"

void *func2_in_thread(void *param) {
    int *i = (int *) param;
    while (*i < 10) {
        printf("Print from thread2 -> %d\n", *i);
        (*i)++;
    }

}


void *func1_in_thread(void *param) {
    int *i = (int *) param;
    while (*i < 10) {
        printf("Print from thread1 -> %d\n", *i);
        (*i)++;
    }
}


void pthreads()
{
    pthread_t pthread1;
    pthread_t pthread2;

    int x = 1;
    int y = 1;

    if (pthread_create(&pthread1, NULL, func1_in_thread, &x)) {
        fprintf(stderr, "Error creating thread\n");
        exit (1);
    }

    if (pthread_create(&pthread2, NULL, func2_in_thread, &y)) {
        fprintf(stderr, "Error creating thread\n");
        exit (1);
    }

    int n = 0;
    while (++n < 10);

    printf("Print from main -> %d\n", n);

    if (pthread_join(pthread1, NULL)) {
        fprintf(stderr, "Error joining thread\n");
        exit (1);
    }

    if (pthread_join(pthread2, NULL)) {
        fprintf(stderr, "Error joining thread\n");
        exit (1);
    }

}